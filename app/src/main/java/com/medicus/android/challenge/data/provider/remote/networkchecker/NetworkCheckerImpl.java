package com.medicus.android.challenge.data.provider.remote.networkchecker;

import android.content.Context;
import android.net.ConnectivityManager;

import java.net.InetAddress;

import javax.inject.Inject;
import javax.inject.Singleton;

import dagger.hilt.android.qualifiers.ApplicationContext;

@Singleton
public class NetworkCheckerImpl implements INetworkChecker {

    private Context context;
    private ConnectivityManager connectivityManager;

    @Inject
    public NetworkCheckerImpl(@ApplicationContext Context context) {
        this.context = context;
        connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
    }

    @Override
    public boolean isConnected() {
        return isInternetAvailable() && connectivityManager.getActiveNetworkInfo().isConnectedOrConnecting();
    }

    private boolean isInternetAvailable() {
        try {
            InetAddress address = InetAddress.getByName("www.google.com");
            return  !address.equals("");
        } catch (Exception e) {
            return false;
        }
    }

}
