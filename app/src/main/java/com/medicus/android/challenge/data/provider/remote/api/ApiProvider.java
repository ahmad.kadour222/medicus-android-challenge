package com.medicus.android.challenge.data.provider.remote.api;

import com.medicus.android.challenge.data.provider.remote.networkchecker.INetworkChecker;
import com.medicus.android.challenge.data.provider.remote.tool.RemoteOnSubscribe;

import javax.inject.Inject;

import io.reactivex.Single;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Class for enqueueing http request using Retrofit calls and depends on RxJava for returned type.
 */
public class ApiProvider {

    private INetworkChecker networkChecker;

    @Inject
    public ApiProvider(INetworkChecker networkChecker) {
        this.networkChecker = networkChecker;
    }

    /**
     * First, check internet status using @{@link INetworkChecker}, then enqueue the call if
     * the internet is available
     *
     * @param call            Retrofit call
     * @param callback        Response callback
     * @param cancelOnDispose Canceling the call onDispose the observable
     * @param <T>             Response data type
     * @return Single
     */
    public <T> Single<T> enqueueCall(Call<T> call, Callback<T> callback, boolean cancelOnDispose) {
        return Single.create(new RemoteOnSubscribe<T>(networkChecker, emitter -> call.enqueue(callback)))
                .doOnDispose(() -> {
                    if (cancelOnDispose && !call.isCanceled())
                        call.cancel();
                });
    }


    /**
     * Same as {enqueueCall(Call<T> call, Callback<T> callback, boolean cancelOnDispose)}
     * but set cancelOnDispose to true by default
     *
     * @param call     Retrofit call
     * @param callback Response callback
     * @param <T>      Response data type
     * @return Single
     */
    public <T> Single<T> enqueueCall(Call<T> call, Callback<T> callback) {
        return enqueueCall(call, callback, true);
    }


    public <T> Single<T> enqueueCall(Call<T> call, boolean cancelOnDispose) {
        return enqueueCall(call, new Callback<T>() {
            @Override
            public void onResponse(Call<T> call, Response<T> response) {

            }

            @Override
            public void onFailure(Call<T> call, Throwable t) {

            }
        }, cancelOnDispose);
    }

}
