package com.medicus.android.challenge.presentation.ui.common.listlivedata;

import androidx.lifecycle.Observer;

import com.medicus.android.challenge.presentation.ui.base.adapter.BaseListAdapter;
import com.medicus.android.challenge.presentation.ui.common.listlivedata.event.ListEvent;


public class ListObserver<T> implements Observer<ListEvent<T>> {

    private final BaseListAdapter<T, ?> adapter;

    public ListObserver(BaseListAdapter<T, ?> adapter) {
        this.adapter = adapter;
    }

    @Override
    public void onChanged(ListEvent<T> event) {
        new ListEventAdapterMediator<>(adapter, event).attach();
    }
}
