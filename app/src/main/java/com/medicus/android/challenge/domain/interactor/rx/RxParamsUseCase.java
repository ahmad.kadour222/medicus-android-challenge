package com.medicus.android.challenge.domain.interactor.rx;

import io.reactivex.Observable;
import io.reactivex.observers.DisposableObserver;

/**
 * Abstract use case with params
 *
 * @param <RESULT> return type
 * @param <PARAMS> params type
 */
abstract public class RxParamsUseCase<PARAMS, RESULT> extends RxBaseUseCase {

    /**
     * Called this method to build the use case observable
     *
     * @param params Use case params
     * @return Use case result as Observable
     */
    protected abstract Observable<RESULT> buildObservable(PARAMS params);

    /**
     * @param PARAMS   Use case params
     * @param observer Observer to register it to the built observable
     */
    public void execute(PARAMS PARAMS, DisposableObserver<RESULT> observer) {
        addDisposable(
                buildObservable(PARAMS)
                        .doOnError(Throwable::printStackTrace)
                        .subscribeOn(getThreadScheduler())
                        .observeOn(getUIScheduler())
                        .subscribeWith(observer)
        );
    }
}
