package com.medicus.android.challenge.data.util.constants;


public final class RemoteConstants {

    public static final class ApiEndPoints {
        private static final String BASE_URL = "v1/";

        public static final String TOKEN_HEADER = "Authorization";
        public static final String LANGUAGE_HEADER = "lang";
        public static final String MOBILE_OS_HEADER = "mobileOS";

    }

    private RemoteConstants() {
    }
}
