package com.medicus.android.challenge.presentation.exception;

import androidx.annotation.StringRes;

public final class ExceptionMessageFactory {
    private ExceptionMessageFactory() {
    }

    /**
     * @param e Exception to return its message
     * @return String resource id (ex: R.string.network_error)
     */
    public static @StringRes int getStringResOf(Exception e) {
        return 0;
    }
}
