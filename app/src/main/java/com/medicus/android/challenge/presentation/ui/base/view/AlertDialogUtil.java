package com.medicus.android.challenge.presentation.ui.base.view;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface.OnClickListener;

public class AlertDialogUtil {


    public static void showDialogMessage(Context context,
                                  String title, String message,
                                  String positiveText, String negativeText,
                                  OnClickListener onPositiveClickListener,
                                  OnClickListener onNegativeClickListener) {

        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        OnClickListener defaultClickListener = (dialogInterface, i) -> dialogInterface.dismiss();

        if (title != null)
            builder = builder.setTitle(title);
        if (message != null)
            builder = builder.setMessage(message);
        if (positiveText != null)
            builder = builder.setPositiveButton(positiveText, onPositiveClickListener == null ? defaultClickListener : onPositiveClickListener);
        if (negativeText != null)
            builder = builder.setNegativeButton(negativeText, onNegativeClickListener == null ? defaultClickListener : onNegativeClickListener);

        if (positiveText == null && negativeText == null)
            builder = builder.setPositiveButton(android.R.string.ok, defaultClickListener);
        builder.show();
    }

    public static void showMessageDialog(Context context,
                                  String title, String message) {
        showDialogMessage(context, title, message, null, null, null, null);
    }
}
