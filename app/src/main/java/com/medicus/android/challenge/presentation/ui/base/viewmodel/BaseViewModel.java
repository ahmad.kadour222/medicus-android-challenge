package com.medicus.android.challenge.presentation.ui.base.viewmodel;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.medicus.android.challenge.domain.interactor.rx.RxBaseUseCase;
import com.medicus.android.challenge.presentation.exception.ExceptionMessageFactory;
import com.medicus.android.challenge.presentation.util.lifecyle.Event;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class BaseViewModel extends ViewModel {

    public MutableLiveData<Boolean> isLoading = new MutableLiveData<>(false);

    private List<RxBaseUseCase> useCases = new ArrayList<>();

    private MutableLiveData<Event<String>> _toastMessage = new MutableLiveData<>();
    public LiveData<Event<String>> toastMessage = _toastMessage;

    private MutableLiveData<Event<Integer>> _toastMessageResource = new MutableLiveData<>();
    public LiveData<Event<Integer>> toastMessageResource = _toastMessageResource;

    private MutableLiveData<Event<Boolean>> _hideKeyboard = new MutableLiveData<>();
    public LiveData<Event<Boolean>> hideKeyboard = _hideKeyboard;

    /**
     * Show toast message from passed string
     *
     * @param message Message to show
     */
    protected void showMessage(String message) {
        _toastMessage.setValue(new Event<>(message));
    }

    /**
     * Show toast message from string resource id
     *
     * @param stringId String resource ID
     */
    protected void showMessage(int stringId) {
        if (stringId > 0)
            _toastMessageResource.setValue(new Event<>(stringId));
    }

    /**
     * Get the exception's message if exists then show it as a toast message.
     *
     * @param error Exception.
     */
    protected void showMessage(Throwable error) {
        if (error.getMessage() != null)
            showMessage(error.getMessage());
        else
            showMessage(ExceptionMessageFactory.getStringResOf((Exception) error));

    }

    protected void hideKeyboard() {
        _hideKeyboard.setValue(new Event<>(true));
    }

    protected boolean isLoading() {
        return isLoading.getValue() != null && isLoading.getValue();
    }

    protected void startLoading() {
        isLoading.setValue(true);
    }

    protected void stopLoading() {
        isLoading.setValue(false);
    }

    /**
     * Register these use cases to dispose them onCleared the ViewModel
     *
     * @param useCases Use Cases to register them.
     */
    protected void registerUseCases(RxBaseUseCase... useCases) {
        this.useCases.addAll(Arrays.asList(useCases));
    }

    @Override
    protected void onCleared() {
        super.onCleared();
        for (RxBaseUseCase useCase : useCases)
            useCase.dispose();
    }
}
