package com.medicus.android.challenge.domain.interactor.rx;

import io.reactivex.Scheduler;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

abstract public class RxBaseUseCase {

    private CompositeDisposable compositeDisposable;

    public RxBaseUseCase() {
        compositeDisposable = new CompositeDisposable();
    }

    /**
     * @return UI Scheduler, you can override it in your use case
     */
    protected Scheduler getUIScheduler() {
        return AndroidSchedulers.mainThread();
    }

    /**
     * @return Background Scheduler, you can override it in your use case
     */
    protected Scheduler getThreadScheduler() {
        return Schedulers.io();
    }

    /**
     * @param disposable Disposable to register it to the composite disposable.
     */
    protected void addDisposable(Disposable disposable) {
        if (disposable != null)
            compositeDisposable.add(disposable);
    }

    /**
     * Dispose all disposables.
     */
    public void dispose() {
        if (compositeDisposable != null && !compositeDisposable.isDisposed())
            compositeDisposable.dispose();
    }
}
