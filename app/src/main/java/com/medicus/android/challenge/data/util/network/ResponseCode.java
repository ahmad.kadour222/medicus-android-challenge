package com.medicus.android.challenge.data.util.network;

public class ResponseCode {
    public static final int SUCCESS = 200;
    public static final int UNAUTHORIZED_USER = 401;
    public static final int EXPIRED_TOKEN = 434;
    public static final int MUST_LOGIN_AGAIN = 436;
    public static final int UNKNOWN_ERROR = 402;
    public static final int UNVERIFIED_ACCOUNT = 406;
    public static final int NETWORK_ERROR = 0;
}
