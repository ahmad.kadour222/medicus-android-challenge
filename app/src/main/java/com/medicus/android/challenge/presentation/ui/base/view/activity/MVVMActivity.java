package com.medicus.android.challenge.presentation.ui.base.view.activity;

import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.databinding.ViewDataBinding;
import androidx.lifecycle.ViewModelProvider;
import androidx.lifecycle.ViewModelStoreOwner;

import com.medicus.android.challenge.presentation.ui.base.viewmodel.BaseViewModel;
import com.medicus.android.challenge.presentation.util.lifecyle.EventObserver;


/**
 * Activity depends on Data-Binding and ViewModel
 * @param <VM> ViewModel Type
 * @param <DATA_BINDING> Data binding type
 */
abstract public class MVVMActivity<VM extends BaseViewModel, DATA_BINDING extends ViewDataBinding> extends BindingActivity<DATA_BINDING> {

    protected VM viewModel;

    /**
     * @return ViewModel variable id (ex: BR.viewModel)
     */
    abstract protected int getViewModelId();

    /**
     * @return ViewModel type (ex: LoginViewModel.class)
     */
    abstract protected Class<VM> getViewModelClass();


    protected ViewModelStoreOwner provideViewModelStoreOwner() {
        return this;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        viewModel = provideViewModel();
        setupBaseObservers();
    }

    protected VM provideViewModel() {
        return new ViewModelProvider(provideViewModelStoreOwner()).get(getViewModelClass());
    }

    protected void setupBaseObservers() {
        if (getViewModelId() > 0)
            binding.setVariable(getViewModelId(), viewModel);
        viewModel.toastMessage.observe(this, new EventObserver<>(this::showMessage));
        viewModel.toastMessageResource.observe(this, new EventObserver<>(this::showMessage));
        viewModel.hideKeyboard.observe(this, new EventObserver<>(ignored -> hideKeyboard()));
    }
}