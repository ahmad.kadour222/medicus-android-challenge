package com.medicus.android.challenge.presentation.ui.common.listlivedata.event;

import com.medicus.android.challenge.presentation.ui.base.adapter.BaseListAdapter;

import java.util.List;

public class RemoveIndexEvent<T> extends ListEvent<T> {

    private final int indexToRemove;

    public RemoveIndexEvent(List<T> allData, int indexToRemove) {
        super(allData);
        this.indexToRemove = indexToRemove;
    }

    @Override
    public void applyEvent(BaseListAdapter<T, ?> adapter) {
        adapter.removeItem(indexToRemove);
    }
}
