package com.medicus.android.challenge.domain.interactor;

public abstract class UseCase<RESULT> {

    public abstract RESULT execute();

}
