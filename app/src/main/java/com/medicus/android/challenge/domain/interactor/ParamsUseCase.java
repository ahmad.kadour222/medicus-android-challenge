package com.medicus.android.challenge.domain.interactor;

public abstract class ParamsUseCase<PARAMS, RESULT> {

    public abstract RESULT execute(PARAMS params);
}
