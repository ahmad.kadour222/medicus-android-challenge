package com.medicus.android.challenge.data.util.network;

import androidx.annotation.Nullable;

import com.medicus.android.challenge.data.exception.NoInternetException;

import static com.medicus.android.challenge.data.util.network.ResponseCode.NETWORK_ERROR;


public final class ResponseExceptionFactory {

    private ResponseExceptionFactory() {
    }

    public static Exception fromCode(int responseCode, @Nullable String message) {
        switch (responseCode) {
            case NETWORK_ERROR:
                return new NoInternetException(message);
            default:
                return new Exception(message);
        }
    }

    public static Exception fromCode(int responseCode) {
        return fromCode(responseCode, null);
    }
}