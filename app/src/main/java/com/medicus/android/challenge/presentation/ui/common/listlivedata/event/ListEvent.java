package com.medicus.android.challenge.presentation.ui.common.listlivedata.event;

import com.medicus.android.challenge.presentation.ui.base.adapter.BaseListAdapter;

import java.util.List;

public abstract class ListEvent<T> {

    private final List<T> allData;

    public ListEvent(List<T> allData) {
        this.allData = allData;
    }

    public List<T> allData() {
        return allData;
    }

    public abstract void applyEvent(BaseListAdapter<T, ?> adapter);
}
