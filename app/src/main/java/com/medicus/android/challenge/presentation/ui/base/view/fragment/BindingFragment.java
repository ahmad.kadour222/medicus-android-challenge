package com.medicus.android.challenge.presentation.ui.base.view.fragment;


import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.databinding.ViewDataBinding;


abstract public class BindingFragment<DB extends ViewDataBinding> extends BaseFragment {

    protected View rootView;
    protected DB binding;


    @Override
    protected void createView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, getLayoutId(), container, false);
        rootView = binding.getRoot();
    }


    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setupDataBinding();
    }

    private void setupDataBinding() {
        if (binding == null)
            return;
        binding.setLifecycleOwner(this);
        binding.executePendingBindings();
    }
}
