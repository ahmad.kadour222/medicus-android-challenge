package com.medicus.android.challenge.presentation.ui.common.listlivedata;

import androidx.lifecycle.MutableLiveData;


import com.medicus.android.challenge.presentation.ui.common.listlivedata.event.AddEvent;
import com.medicus.android.challenge.presentation.ui.common.listlivedata.event.ListEvent;
import com.medicus.android.challenge.presentation.ui.common.listlivedata.event.RemoveIndexEvent;

import java.util.ArrayList;
import java.util.List;

public class ListLiveData<T> extends MutableLiveData<ListEvent<T>> {

    private final List<T> data = new ArrayList<>();


    public void add(T item) {
        data.add(item);

        List<T> dataToAdd = new ArrayList<>();
        dataToAdd.add(item);

        setValue(new AddEvent<>(data, dataToAdd));
    }

    public void addAll(List<T> items) {
        data.addAll(items);
        setValue(new AddEvent<>(data, items));
    }

    public void remove(int index) {
        data.remove(index);
        setValue(new RemoveIndexEvent<>(data, index));
    }

}
