package com.medicus.android.challenge.domain.interactor.rx;

import io.reactivex.Observable;
import io.reactivex.observers.DisposableObserver;

/**
 * Abstract use case without params
 *
 * @param <T> return type
 */
abstract public class RxUseCase<T> extends RxBaseUseCase {

    /**
     * Called this method to build the use case observable
     *
     * @return Use case result as Observable
     */
    protected abstract Observable<T> buildObservable();

    /**
     * @param observer Observer to register it to the built observable
     */
    public void execute(DisposableObserver<T> observer) {
        addDisposable(
                buildObservable()
                        .doOnError(Throwable::printStackTrace)
                        .subscribeOn(getThreadScheduler())
                        .observeOn(getUIScheduler())
                        .subscribeWith(observer)
        );
    }
}
