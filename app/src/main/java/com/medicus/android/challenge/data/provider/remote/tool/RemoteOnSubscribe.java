package com.medicus.android.challenge.data.provider.remote.tool;

import com.medicus.android.challenge.data.exception.NoInternetException;
import com.medicus.android.challenge.data.provider.remote.networkchecker.INetworkChecker;

import io.reactivex.SingleEmitter;
import io.reactivex.SingleOnSubscribe;

/**
 * Helper class to check the internet connection before make a request using RxJava
 * @param <T> Remote Data type
 *
 */
public class RemoteOnSubscribe<T> implements SingleOnSubscribe<T> {

    private INetworkChecker networkChecker;
    private OnConnected<T> onConnected;

    public RemoteOnSubscribe(INetworkChecker networkChecker, OnConnected<T> onConnected) {
        this.networkChecker = networkChecker;
        this.onConnected = onConnected;
    }

    @Override
    public void subscribe(SingleEmitter<T> emitter) throws Exception {
        if (!networkChecker.isConnected()) {
            emitter.onError(new NoInternetException());
        } else {
            onConnected.onConnect(emitter);
        }
    }

    public interface OnConnected<T> {
        void onConnect(SingleEmitter<T> emitter) throws Exception;
    }
}
