package com.medicus.android.challenge.data.provider.remote.tool;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RemoteCallback<T> implements Callback<T> {
    @Override
    public void onResponse(Call<T> call, Response<T> response) {

    }

    @Override
    public void onFailure(Call<T> call, Throwable t) {

    }

    //
//    private static JsonMapper jsonMapper = new JsonMapper();
//
//    private OnResponse<T> onResponse;
//
//    private OnError<T> onError;
//
//    public RemoteCallback(OnResponse<T> onResponse, OnError<T> onError) {
//        this.onResponse = onResponse;
//        this.onError = onError;
//    }
//
//    public RemoteCallback(OnResponse<T> onResponse) {
//        this(onResponse, null);
//    }
//
//    @Override
//    public void onResponse(Call<T> call, Response<T> response) {
//        if (onResponse != null) {
//            // check if the retrofit response is successful
//            if (response.isSuccessful()) {
//                handelSuccessfulResponse(response, call);
//            } else {
//                try {
//                    BaseResponse errorResponse = jsonMapper.unmap(response.errorBody().string(), BaseResponse.class);
//                    handleUnSuccessfulResponse(errorResponse, call);
//                } catch (Exception e) {
//                    onFailure(call, e);
//                }
//            }
//        }
//    }
//
//    private void handelSuccessfulResponse(Response<T> response, Call<T> call) {
//        BaseResponse baseResponse = (BaseResponse) response.body();
//        // check if the API response is successful
//        if (baseResponse.isSuccessful()) { // if statusCode == 200
//            onResponse.onResponse(true, baseResponse.getStatusCode(), response.body(), null, null, call);
//        } else {
//            handleUnSuccessfulResponse(baseResponse, call);
//        }
//    }
//
//    private void handleUnSuccessfulResponse(BaseResponse errorResponse, Call<T> call) {
//        Exception e = ResponseExceptionFactory.fromCode(errorResponse.getStatusCode(), errorResponse.getMessage());
//        onResponse.onResponse(false, errorResponse.getStatusCode(), null, errorResponse, e, call);
//    }
//
//    @Override
//    public void onFailure(Call<T> call, Throwable t) {
//        t.printStackTrace();
//        if (onError != null)
//            onError.onError(call, t);
//        if (onResponse != null)
//            onResponse.onResponse(false, ResponseCode.NETWORK_ERROR, null, null,
//                    ResponseExceptionFactory.fromCode(ResponseCode.NETWORK_ERROR), call);
//    }
//
//    public interface OnResponse<T> {
//        /**
//         * @param responseCode response code
//         * @param body         response body (null onError)
//         * @param errorBody    BaseResponse (null onSuccessful or null on NoInternetException)
//         * @param error        Exception fetched from @{ResponseExceptionFactory} and its message is @{BaseResponse.message}
//         * @param call         Retrofit call
//         */
//        void onResponse(
//                boolean isSuccessful,
//                int responseCode,
//                @Nullable T body,
//                @Nullable BaseResponse errorBody,
//                @Nullable Exception error,
//                Call<T> call
//        );
//    }
//
//    public interface OnError<T> {
//        void onError(Call<T> call, Throwable t);
//    }
}
