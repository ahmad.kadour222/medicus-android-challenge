package com.medicus.android.challenge.domain;

import androidx.annotation.Nullable;

/**
 * Data wrapper
 *
 * @param <T> data type
 */
public class Result<T> {

    @Nullable
    private T data;

    private Exception exception;

    private Status status;

    public Result(@Nullable T data, Exception exception, Status status) {
        this.data = data;
        this.exception = exception;
        this.status = status;
    }

    public static <T> Result<T> success(T data) {
        return new Result<>(data, null, Status.SUCCESS);
    }

    public static <T> Result<T> error(T data, Exception error) {
        return new Result<>(data, error, Status.ERROR);
    }

    public static <K, T> Result<K> mapData(Result<T> result, ResultMapper<T, K> mapper) {
        return new Result<>(mapper.map(result.getData()), result.getException(), result.getStatus());
    }



    public T getData() {
        return data;
    }

    public Exception getException() {
        return exception;
    }

    public Status getStatus() {
        return status;
    }

    public boolean isSuccess() {
        return status == Status.SUCCESS;
    }

    public void setData(T data) {
        this.data = data;
    }

    public <K> Result<K> mapData(ResultMapper<T, K> mapper) {
        return mapData(this, mapper);
    }

    public enum Status {
        SUCCESS, ERROR
    }

    public interface ResultMapper<T, K> {
        K map(T data);
    }
}
