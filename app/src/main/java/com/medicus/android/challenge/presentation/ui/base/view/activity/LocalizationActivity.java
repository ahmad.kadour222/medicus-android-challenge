package com.medicus.android.challenge.presentation.ui.base.view.activity;

import android.content.Context;
import android.content.res.Resources;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;


import com.medicus.android.challenge.presentation.util.localization.LocaleManager;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;


/**
 * Activity supports localization changing.
 */
abstract public class LocalizationActivity extends AppCompatActivity {

    private String currentLanguage;
    private Disposable localeDisposable;

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(LocaleManager.getInstance(this).setLocale(newBase));
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setupLanguageChecker();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (localeDisposable != null && !localeDisposable.isDisposed())
            localeDisposable.dispose();
    }

    private void setupLanguageChecker() {
        currentLanguage = LocaleManager.getInstance(this).getSavedLanguage();
        localeDisposable = LocaleManager.getInstance(this).observable()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(resources -> {
                    checkLanguageChanging();
                });
    }

    private void checkLanguageChanging() {
        String newLanguage = LocaleManager.getInstance(this).getSavedLanguage();
        if (!newLanguage.equals(currentLanguage)) {
            onLanguageChanged(LocaleManager.getInstance(this).setLocale(this).getResources());
        }
        currentLanguage = newLanguage;
    }

    /**
     * This method is called when the app language changes.
     * @param newResources New locale resources
     */
    protected void onLanguageChanged(Resources newResources) {
    }
}
