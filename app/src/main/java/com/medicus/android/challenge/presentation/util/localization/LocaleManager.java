package com.medicus.android.challenge.presentation.util.localization;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Build;
import android.os.LocaleList;

import androidx.annotation.RequiresApi;

import java.util.LinkedHashSet;
import java.util.Locale;
import java.util.Set;

import io.reactivex.subjects.PublishSubject;

import static android.os.Build.VERSION_CODES.LOLLIPOP;
import static android.os.Build.VERSION_CODES.N;

final public class LocaleManager {

    private static String LANGUAGE_KEY = "app_language";
    private static String DEFAULT_SHARED_PREFERENCE_NAME = "language_shared_preferences";

    public static String savedLanguage = LanguagesCode.English;

    private static LocaleManager INSTANCE = null;

    private SharedPreferences sharedPreferences;
    private PublishSubject<Resources> publishSubject = PublishSubject.create();

    private LocaleManager(Context context, String sharedPreferencesName) {
        sharedPreferences = context.getSharedPreferences(sharedPreferencesName, Context.MODE_PRIVATE);
    }

    synchronized public static LocaleManager getInstance(Context context) {
        if (INSTANCE == null) {
            INSTANCE = new LocaleManager(context, DEFAULT_SHARED_PREFERENCE_NAME);
        }
        return INSTANCE;
    }

    public static Locale getLocale(Resources res) {
        Configuration config = res.getConfiguration();
        return Build.VERSION.SDK_INT >= N ? config.getLocales().get(0) : config.locale;
    }

    public Context setLocale(Context c) {
        return updateResources(c, getSavedLanguage());
    }

    /**
     * @param c            Context
     * @param languageCode language code (ex: 'en', 'ar', ...), you can see {@link LanguagesCode}
     * @return
     */
    public Context setNewLocale(Context c, String languageCode) {
        saveLanguage(languageCode);
        return updateResources(c, languageCode);
    }

    public String getSavedLanguage() {
        return sharedPreferences.getString(LANGUAGE_KEY, LanguagesCode.English);
    }

    @SuppressLint("ApplySharedPref")
    private void saveLanguage(String newLanguage) {
        sharedPreferences.edit().putString(LANGUAGE_KEY, newLanguage).commit();
    }

    private Context updateResources(Context context, String language) {
        Locale locale = new Locale(language);
        Locale.setDefault(locale);

        Resources res = context.getResources();
        Configuration config = new Configuration(res.getConfiguration());
        if (Build.VERSION.SDK_INT >= N) {
            setLocaleForApi24(config, locale);
            config.setLayoutDirection(locale);
            context = context.createConfigurationContext(config);
        } else if (Build.VERSION.SDK_INT >= LOLLIPOP) {
            config.setLayoutDirection(locale);
            config.setLocale(locale);
            context = context.createConfigurationContext(config);
        } else {
            config.locale = locale;
            res.updateConfiguration(config, res.getDisplayMetrics());
        }
        publishSubject.onNext(context.getResources());
        savedLanguage = language;
        return context;
    }

    @RequiresApi(api = N)
    private void setLocaleForApi24(Configuration config, Locale target) {
        Set<Locale> set = new LinkedHashSet<>();
        // bring the target locale to the front of the list
        set.add(target);

        LocaleList all = LocaleList.getDefault();
        for (int i = 0; i < all.size(); i++) {
            // append other locales supported by the user
            set.add(all.get(i));
        }

        Locale[] locales = set.toArray(new Locale[0]);
        config.setLocales(new LocaleList(locales));
    }

    public PublishSubject<Resources> observable() {
        return publishSubject;
    }
}
