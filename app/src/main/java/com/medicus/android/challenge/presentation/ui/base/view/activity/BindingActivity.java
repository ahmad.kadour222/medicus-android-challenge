package com.medicus.android.challenge.presentation.ui.base.view.activity;

import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.databinding.ViewDataBinding;

/**
 * Activity depends on Data-Binding
 */
public abstract class BindingActivity<DATA_BINDING extends ViewDataBinding> extends BaseActivity {

    protected DATA_BINDING binding;

    @Override
    protected void setView() {
        binding = DataBindingUtil.setContentView(this, getLayoutId());
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setupDataBinding();
    }

    protected void setupDataBinding() {
        binding.setLifecycleOwner(this);
        binding.executePendingBindings();
    }
}
