package com.medicus.android.challenge.data.provider.remote.networkchecker;

public interface INetworkChecker {
    boolean isConnected();
}
