package com.medicus.android.challenge.presentation.ui.common.listlivedata;

import com.medicus.android.challenge.presentation.ui.base.adapter.BaseListAdapter;
import com.medicus.android.challenge.presentation.ui.common.listlivedata.event.ListEvent;

public class ListEventAdapterMediator<T> {
    private final BaseListAdapter<T, ?> adapter;
    private final ListEvent<T> event;

    public ListEventAdapterMediator(BaseListAdapter<T, ?> adapter, ListEvent<T> event) {
        this.adapter = adapter;
        this.event = event;
    }

    public void attach() {
        if (adapter.isEmptyOrNullData())
            adapter.submitData(event.allData());
        else
            event.applyEvent(adapter);

    }
}
