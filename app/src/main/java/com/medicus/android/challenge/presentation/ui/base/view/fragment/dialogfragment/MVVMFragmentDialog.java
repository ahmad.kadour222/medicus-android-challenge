package com.medicus.android.challenge.presentation.ui.base.view.fragment.dialogfragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.ViewDataBinding;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.ViewModelProvider;
import androidx.lifecycle.ViewModelStoreOwner;

import com.medicus.android.challenge.presentation.ui.base.viewmodel.BaseViewModel;
import com.medicus.android.challenge.presentation.util.lifecyle.EventObserver;

public abstract class MVVMFragmentDialog <VM extends BaseViewModel, DB extends ViewDataBinding> extends BaseFragmentDialog<DB>  {
    protected VM viewModel;

    abstract protected Class<VM> getViewModelClass();

    abstract protected int getViewModelId();

    protected ViewModelStoreOwner getViewModelOwner() {
        return this;
    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        viewModel = provideViewModel();
    }

    protected VM provideViewModel() {
        return new ViewModelProvider(getViewModelOwner()).get(getViewModelClass());
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = super.onCreateView(inflater, container, savedInstanceState);
        setupBaseObservers();
        return rootView;
    }


    protected void setupBaseObservers() {
        if (getViewModelId() > 0)
            binding.setVariable(getViewModelId(), viewModel);
        LifecycleOwner owner = getViewLifecycleOwner();
        viewModel.toastMessageResource.observe(owner, new EventObserver<>(this::showMessage));
        viewModel.toastMessage.observe(owner, new EventObserver<>(this::showMessage));
        viewModel.hideKeyboard.observe(owner, new EventObserver<>(ignored -> hideKeyboard()));
    }
}
