package com.medicus.android.challenge.presentation.util.databinding;

import android.graphics.drawable.Drawable;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.databinding.BindingAdapter;
import androidx.recyclerview.widget.RecyclerView;

import com.medicus.android.challenge.presentation.ui.base.adapter.BaseListAdapter;
import com.medicus.android.challenge.presentation.ui.common.listlivedata.ListEventAdapterMediator;
import com.medicus.android.challenge.presentation.ui.common.listlivedata.event.ListEvent;
import com.medicus.android.challenge.presentation.util.images.ImagesUtil;


public final class DataBindingAdapters {

    private DataBindingAdapters() {
    }

    @BindingAdapter("android:visibility")
    public static void viewVisibility(View view, boolean isVisible) {
        view.setVisibility(isVisible ? View.VISIBLE : View.INVISIBLE);
    }

    @BindingAdapter("existence")
    public static void viewExistence(View view, boolean isExists) {
        view.setVisibility(isExists ? View.VISIBLE : View.GONE);
    }

    @BindingAdapter(value = {"android:src", "placeHolder"}, requireAll = false)
    public static void loadImage(ImageView imageView, String url, Drawable placeHolder) {
        ImagesUtil.loadPictureAndCache(
                imageView,
                url,
                placeHolder
        );
    }

    @BindingAdapter("android:text")
    public static void integerText(TextView textView, Number number) {
        textView.setText(number.toString());
    }

    @BindingAdapter("observeListEvents")
    public static void loadData(RecyclerView recyclerView, ListEvent listEvent) {
        RecyclerView.Adapter<?> adapter = recyclerView.getAdapter();
        if (adapter instanceof BaseListAdapter) {
            new ListEventAdapterMediator((BaseListAdapter) adapter, listEvent).attach();
        }
    }
}
