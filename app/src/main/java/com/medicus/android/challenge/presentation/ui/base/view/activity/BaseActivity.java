package com.medicus.android.challenge.presentation.ui.base.view.activity;

import android.content.DialogInterface;
import android.content.res.Resources;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Toast;

import androidx.annotation.DrawableRes;
import androidx.annotation.Nullable;

import com.medicus.android.challenge.presentation.ui.base.view.AlertDialogUtil;
import com.medicus.android.challenge.presentation.ui.base.view.IBaseView;

abstract public class BaseActivity extends LocalizationActivity implements IBaseView {

    private boolean isRunning;


    protected abstract int getLayoutId();

    @Override
    protected void onResume() {
        super.onResume();
        isRunning = true;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setView();
    }


    protected void setView() {
        setContentView(getLayoutId());
    }

    @Override
    protected void onPause() {
        super.onPause();
        isRunning = false;
    }


    // checks if the activity running now (not on foreground or background)
    protected boolean isActivityRunning() {
        return isRunning;
    }


    @Override
    protected void onLanguageChanged(Resources newResources) {
        recreate();
    }


    // base methods

    @Override
    public void showMessage(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void showMessage(int stringId) {
        showMessage(getString(stringId));
    }

    @Override
    public void showDialogMessage(String title, String message, String positiveText, String negativeText,
                                  DialogInterface.OnClickListener positiveListener, DialogInterface.OnClickListener negativeListener) {
        AlertDialogUtil.showDialogMessage(this, title, message, positiveText, negativeText, positiveListener, negativeListener);
    }

    @Override
    public void showDialogMessage(String title, String message) {
        AlertDialogUtil.showMessageDialog(this, title, message);
    }

    @Override
    public void showDialogMessage(int title, int message) {
        showDialogMessage(getString(title), getString(message));
    }

    @Override
    public void showDialogMessage(String message) {
        showDialogMessage("", message);
    }

    @Override
    public void showDialogMessage(int message) {
        showDialogMessage("", getString(message));
    }

    @Override
    public void hideKeyboard() {
        View currentFocus = getCurrentFocus();
        if (currentFocus == null)
            currentFocus = new View(this);
        InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(currentFocus.getWindowToken(), 0);
    }

    public void changeToolbarTitle(String title) {
        if (getSupportActionBar() != null)
            getSupportActionBar().setTitle(title);
    }

    public void setFullScreen() {
        Window w = getWindow();
        w.setFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS, WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS);
    }

    public void changeHomeIndicator(@DrawableRes int drawable) {
        if (getSupportActionBar() != null)
            getSupportActionBar().setHomeAsUpIndicator(drawable);
    }
}
