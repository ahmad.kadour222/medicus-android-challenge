package com.medicus.android.challenge.presentation.ui.base.view;

import android.content.DialogInterface;

import androidx.annotation.StringRes;

public interface IBaseView {
    void showMessage(String message);

    void showMessage(@StringRes int stringId);

    void showDialogMessage(String title, String message,
                           String positiveText, String negativeText,
                           DialogInterface.OnClickListener onPositiveClickListener,
                           DialogInterface.OnClickListener onNegativeClickListener);

    void showDialogMessage(String title, String message);

    void showDialogMessage(@StringRes int title, @StringRes int message);

    void showDialogMessage(String message);

    void showDialogMessage(@StringRes int message);

    void hideKeyboard();
}
