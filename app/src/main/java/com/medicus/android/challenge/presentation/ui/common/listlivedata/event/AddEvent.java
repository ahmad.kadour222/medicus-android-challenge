package com.medicus.android.challenge.presentation.ui.common.listlivedata.event;

import com.medicus.android.challenge.presentation.ui.base.adapter.BaseListAdapter;

import java.util.List;

public class AddEvent<T> extends ListEvent<T> {

    private final List<T> dataToAdd;

    public AddEvent(List<T> allData, List<T> dataToAdd) {
        super(allData);
        this.dataToAdd = dataToAdd;
    }

    @Override
    public void applyEvent(BaseListAdapter<T, ?> adapter) {
        adapter.insertData(dataToAdd);
    }
}
