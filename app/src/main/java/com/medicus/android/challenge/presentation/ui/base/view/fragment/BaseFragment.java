package com.medicus.android.challenge.presentation.ui.base.view.fragment;

import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.medicus.android.challenge.presentation.ui.base.view.AlertDialogUtil;
import com.medicus.android.challenge.presentation.ui.base.view.IBaseView;


public abstract class BaseFragment extends Fragment implements IBaseView {


    protected View rootView;

    protected abstract int getLayoutId();

    protected void createView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(getLayoutId(), container, false);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        createView(inflater, container, savedInstanceState);
        return rootView;
    }

    public void onBackPressed() {
        requireActivity().onBackPressed();
    }

    // Base methods

    @Override
    public void showMessage(String message) {
        Toast.makeText(getContext(), message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void showMessage(int stringId) {
        showMessage(getString(stringId));
    }


    @Override
    public void showDialogMessage(String title, String message, String positiveText, String negativeText,
                                  DialogInterface.OnClickListener positiveListener, DialogInterface.OnClickListener negativeListener) {
        AlertDialogUtil.showDialogMessage(requireContext(), title, message, positiveText, negativeText, positiveListener, negativeListener);
    }

    @Override
    public void showDialogMessage(String title, String message) {
        AlertDialogUtil.showMessageDialog(requireContext(), title, message);
    }

    @Override
    public void showDialogMessage(int title, int message) {
        showDialogMessage(getString(title), getString(message));
    }

    @Override
    public void showDialogMessage(String message) {
        showDialogMessage("", message);
    }

    @Override
    public void showDialogMessage(int message) {
        showDialogMessage("", getString(message));
    }

    @Override
    public void hideKeyboard() {
        View currentFocus = getActivity().getCurrentFocus();
        if (currentFocus == null)
            currentFocus = new View(getContext());
        InputMethodManager inputMethodManager = (InputMethodManager) getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(currentFocus.getWindowToken(), 0);
    }
}
